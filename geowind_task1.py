# MTMW12 assignment 3. Vitor Lavor October 2021
# Python code to numerically differentiate the pressure in order to calculate
# the geostrophic wind direction using 2-point differencing and
# compare with the analytic solution and plot.

import numpy as np
import matplotlib.pyplot as plt
from utils import *


# Resolution and size of the domain
N = 10                     # Number of intervals
ymin = properties['ymin']
ymax = properties['ymax']
dy = (ymax - ymin)/N       # Length of the spacings

# The spatial dimension, y:
y = np.linspace(ymin, ymax, N+1)

# Arrays for the pressure, and exact geostrophic wind
p = pressure(y, properties)
uExact = uGeoExact(y, properties)

# Pressure gradient, and the wind calculated by numerically differentiating
# the pressure
dpdy = gradient_2point(p, dy)
uNum = geoWind(dpdy, properties)

# Graph to compare the numerical and analytic solutions
# plot using large fonts
font = {'size': 14}
plt.rc('font', **font)

# Plot the approximate and exact wind at y points
plt.plot(y/1000, uExact, 'k-', label='Analytical solution')
plt.plot(y/1000, uNum, '*k--', label='Two-point approximation',
         ms=12, markeredgewidth=1.5, markerfacecolor='none')
plt.legend(loc='best')
plt.xlabel('y (km)')
plt.ylabel('u (m/s)')
plt.tight_layout()
plt.savefig('geoWind.pdf')
plt.show()

# Plot the errors
plt.plot(y/1000, uNum - uExact, '*k--', label='Two-point approximation',
         ms=12, markeredgewidth=1.5, markerfacecolor='none')
plt.legend(loc='best')
plt.axhline(linestyle='-', color='k')
plt.xlabel('y (km)')
plt.ylabel('u error (m/s)')
plt.tight_layout()
plt.savefig('geoWindErrors.pdf')
plt.show()

