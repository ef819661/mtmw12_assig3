# MTMW12 assignment 3. Vitor Lavor October 2021
# Python code to numerically differentiate the pressure in order to calculate
# the geostrophic wind direction using 2-point differencing and
# Calculate the order of accuracy of numerical methods and plot.

import numpy as np
import matplotlib.pyplot as plt
from utils import *

def OrderAccuracy():
    """
    Calculates the order of accuracy and plot
    """
    # Resolution and size of the domain
    ymin = properties['ymin']
    ymax = properties['ymax']

    # Resolution and size of the domain
    N = np.array([10, 20, 40, 80, 160])     # Number of intervals
    dy = (ymax - ymin)/N                    # Length of the spacings

    # Initialise the errors array
    errors1 = np.zeros(len(N))
    errors2 = np.zeros(len(N))

    # Calculate errors for each resolution
    for i in range(len(N)):
        y = np.linspace(ymin, ymax, N[i]+1)
        p = pressure(y, properties)
        dpdy = gradient_2point(p, dy[i])
        u = geoWind(dpdy, properties)
        uExact = uGeoExact(y, properties)

        # Errors for end point
        errors1[i] = abs(u[-1] - uExact[-1])

        # Errors for middle point
        y_5 = 5e5
        assert y_5 in y
        y_index = int(N[i]/2)
        errors2[i] = abs(u[y_index] - uExact[y_index])

    # Plot the errors as a function of resolution (on log-log scale)
    font = {'size' : 14}
    plt.rc('font', **font)
    plt.loglog(dy, errors1, '*k--',  label='at $y=1000$ km')
    plt.loglog(dy, errors2, 'ok:', label='at $y=500$ km')
    plt.legend(loc='best')
    plt.xlabel('$\Delta y$ (m)')
    plt.ylabel('error (m/s)')
    plt.tight_layout()
    plt.savefig('order.pdf')
    plt.show()

OrderAccuracy()