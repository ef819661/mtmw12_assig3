# Physical properties and functions for calculations the gerostrophic wind

import numpy as np

properties = {
    'pa': 1e5,  # Mean pressure
    'pb': 200,  # Magnitude of the pressure variation
    'f': 1e-4,  # Coriolis parameter
    'rho': 1,  # Density
    'L': 2.4e6,  # Lenght scale of the pressure variations (k)
    'ymin': 0,  # Start of the t domain
    'ymax': 1e6  # End of the y domain
}


def pressure(y, props):
    """ The pressure at a given location.

    :param y: location [m]
    :param props: dict of properties
    :return: pressure
    """

    pa = props['pa']
    pb = props['pb']
    L = props['L']
    return pa + pb * np.cos(y * np.pi / L)


def uGeoExact(y, props):
    """ The analytical solution for geostrophic wind at a given location.

    :param y: location [m]
    :param props: dict of properties
    :return: geostrophic wind [m/s]
    """

    pb = props['pb']
    rho = props['rho']
    f = props['f']
    L = props['L']
    return pb*np.pi*np.sin(y*np.pi/L) / (rho*f*L)


def geoWind(dfdy, props):
    """ Calculates the geostrophic wind as a function of pressure gradient
    based on dictionary of physical properties.

    :param dfdy: gradient
    :param props: dict of properties
    :return: geostrophic wind [m/s]
    """

    rho = props["rho"]
    f = props["f"]
    return - dfdy/(rho*f)


## Differentiation Methods

def gradient_2point(f, dx):
    """ Gradient of one dimensional array using 2-point centered differences

    :param f: array of points
    :param dx:
    :return: array of results
    """

    # Initialise the array
    dfdx = np.zeros_like(f)

    # Start and End points
    dfdx[0] = (f[1] - f[0]) / dx
    dfdx[-1] = (f[-1] - f[-2]) / dx

    for i in range(1, len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1]) / (2*dx)

    return dfdx


def gradient_2ndOrder(f, dx):
    """Gradient of one dimensional array using 2nd order accuracy method
    for end-points.

    :param f: array of points
    :param dx:
    :return: array of results
    """

    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    # Second order gradients at the end points
    dfdx[0] = (-f[2] + 4 * f[1] - 3 * f[0]) / (2 * dx)
    dfdx[-1] = (3 * f[-1] - 4 * f[-2] + f[-3]) / (2 * dx)
    # Centred differences for the mid-points
    for i in range(1, len(f) - 1):
        dfdx[i] = (f[i + 1] - f[i - 1]) / (2 * dx)
    return dfdx